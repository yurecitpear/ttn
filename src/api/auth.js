import httpClient from "./httpClient.js";

const url = 'auth'

export const authApi = {


  async doLogin(data) {
    try {
      const resp = await httpClient.post(`${url}/login`, {
        email: data.email,
        password: data.password,
      })

      console.log(resp.data)
      localStorage.setItem('token', resp.data.accessToken)
      localStorage.setItem('user', JSON.stringify(resp.data.user))
      return resp.data.token

    } catch (err) {
      throw err
    }
  },

  async doRegister(data) {
    try {
      const resp = await httpClient.post(`${url}/register`, {
        name: data.name,
        username: data.username,
        email: data.email,
        phone: data.phone,
        password: data.password,
      })
      localStorage.setItem('token', resp.data.token)
      localStorage.setItem('user', JSON.stringify(resp.data.user))
      return resp.data.token

    } catch (err) {
      throw err
    }
  },

  resetPass(mail) {
    try {
      return httpClient.post(`${url}/password/reset`, {
        email: mail
      }).then(response => {
        return response
      })
    } catch (err) {
      console.log(err)
    }
  },

  getEmailVerified() {
    try {
      return httpClient.post(`${url}/email/resend`)
      .then(( response ) => {
        return response
      })
    } catch(err) {
      console.log(err)
    }
  },

}
