import httpClient from "./httpClient.js";
const url = 'users'



export const userApi = {

  async getAll() {
    try {
      const resp = await httpClient.get(`${url}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async getBtId(id) {
    try {
      const resp = await httpClient.get(`${url}/${id}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async create(data) {

    try {
      const resp = await httpClient.post(`${url}`, data)

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async update(id, data) {
    try {
      const resp = await httpClient.put(`${url}/profile/${id}`, data)
      return resp.data

    } catch (err) {
      throw err
    }
  },

  async del(id) {
    try {
      const resp = await httpClient.delete(`${url}/${id}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

}
