import httpClient from "./httpClient.js";
const url = 'additions'

export const additionApi = {

  async getAll() {
    try {
      const resp = await httpClient.get(`${url}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async getSubcategories(id) {
    try {
      const resp = await httpClient.get(`${url}/${id}/subcategories`)
      return resp.data

    } catch (err) {
      throw err
    }
  },

  async getBtId(id) {
    try {
      const resp = await httpClient.get(`${url}/${id}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async searchByName(string) {
    try {
      const resp = await httpClient.get(`${url}?n=${string}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async create(data) {
    try {
      const resp = await httpClient.post(`${url}`, {
        name: data.name,
        price: Number(data.price)
      })

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async update(data) {
    try {
      const resp = await httpClient.put(`${url}/${data.id}`, {
        name: data.name,
        price: Number(data.price)
      })
      return resp.data

    } catch (err) {
      throw err
    }
  },

  async del(id) {
    try {
      const resp = await httpClient.delete(`${url}/${id}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

}
