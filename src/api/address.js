import httpClient from "./httpClient.js";
const url = 'address';

export const addressApi = {

  // Получение адресов с пагинацией и фильтрацией по имени
  async getAll(params) {
    try {
      // Фильтруем параметры, исключая null, undefined и пустые строки
      const filteredParams = {};
      for (const key in params) {
        if (params[key] !== null && params[key] !== undefined && params[key] !== '') {
          filteredParams[key] = params[key];
        }
      }

      const resp = await httpClient.get(`${url}`, {
        params: filteredParams,
      });

      return resp.data;

    } catch (err) {
      throw err;
    }
  },

  // Получение адреса по ID
  async getById(id) {
    try {
      const resp = await httpClient.get(`${url}/${id}`);

      return resp.data;

    } catch (err) {
      throw err;
    }
  },

  // Создание нового адреса
  async create(data) {
    try {
      const requestBody = {
        name: data.name,
        km: Number(data.km),
        pricePerCubic: Number(data.pricePerCubic),
        priceFor8Cubic: Number(data.priceFor8Cubic),
      };
      if (data.coordinates) { requestBody.coordinates = data.coordinates; }
      if (data.factory?.id) { requestBody.factoryId = data.factory.id; }

      const resp = await httpClient.post(`${url}`, requestBody);

      return resp.data;

    } catch (err) {
      throw err;
    }
  },

  // Обновление существующего адреса
  async update(data) {
    try {
      const requestBody = {
        name: data.name,
        km: Number(data.km),
        pricePerCubic: Number(data.pricePerCubic),
        priceFor8Cubic: Number(data.priceFor8Cubic),
      };
      if (data.coordinates) { requestBody.coordinates = data.coordinates; }
      if (data.factory?.id) { requestBody.factoryId = data.factory.id; }

      const resp = await httpClient.put(`${url}/${data.id}`, requestBody);

      return resp.data;

    } catch (err) {
      throw err;
    }
  },

  // Удаление адреса
  async del(id) {
    try {
      const resp = await httpClient.delete(`${url}/${id}`);

      return resp.data;

    } catch (err) {
      throw err;
    }
  },

};
