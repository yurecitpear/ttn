import httpClient from "./httpClient.js";
const url = 'factory-prices'

export const factoryPriceApi = {

  async getAll() {
    try {
      const resp = await httpClient.get(`${url}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async getPriceProductFactory(params) {
    try {
      const filteredParams = {}

      for (const key in params) {
        if (params[key] !== null && params[key] !== '') {
          filteredParams[key] = params[key]
        }
      }

      const resp = await httpClient.get(`${url}/by-factory-product`, {
        params: filteredParams,
      })

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async getPriceTable(data, searchData) {
    let params = ``
    if (!!searchData.factory?.id) params += `&factoryId=${searchData.factory?.id}`
    if (!!searchData.category?.id) params += `&categoryId=${searchData.category?.id}`
    if (!!searchData.subcategory?.id) params += `&subcategoryId=${searchData.subcategory?.id}`

    try {
      const resp = await httpClient.get(`${url}/price-table?page=${data.current}&perPage=15${params}`)
      return resp.data

    } catch (err) {
      throw err
    }
  },

  async getBtId(id) {
    try {
      const resp = await httpClient.get(`${url}/${id}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async create(data) {
    try {
      const resp = await httpClient.post(`${url}`, {
        factoryId: data.factory?.id,
        productId: data.product?.id,
        priceNal: data.priceNal,
        priceBeznal: data.priceBeznal,
      })

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async update(data) {
    try {
      const resp = await httpClient.put(`${url}/${data.factoryPriceId}`, {
        factoryId: data.factory?.id,
        productId: data.product?.id,
        priceNal: data.priceNal,
        priceBeznal: data.priceBeznal,
      })
      return resp.data

    } catch (err) {
      throw err
    }
  },

  async del(id) {
    try {
      const resp = await httpClient.delete(`${url}/${id}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

}
