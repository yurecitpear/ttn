import httpClient from "./httpClient.js";
const url = 'subcategories'

export const subcategoriesApi = {

  async getAll() {
    try {
      const resp = await httpClient.get(`${url}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async getBtId(id) {
    try {
      const resp = await httpClient.get(`${url}/${id}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async getprductsBySubcategory(id) {
    try {
      const resp = await httpClient.get(`${url}/${id}/products`)
      return resp.data

    } catch (err) {
      throw err
    }
  },

  async create(data) {
    try {
      const resp = await httpClient.post(`${url}`, {
        name: data.name,
        categoryId: data.category.id
      })

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async update(data) {
    try {
      const resp = await httpClient.put(`${url}/${data.id}`, {
        name: data.name,
        categoryId: data.category.id
      })
      return resp.data

    } catch (err) {
      throw err
    }
  },

  async del(id) {
    try {
      const resp = await httpClient.delete(`${url}/${id}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

}
