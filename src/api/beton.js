import httpClient from "./httpClient.js";
const url = 'concrete'

export const betonApi = {
    
  async getAll() {
    try {
      const resp = await httpClient.get(`${url}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async getBtId(id) {
    try {
      const resp = await httpClient.get(`${url}/${id}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async create(data) {
    try {
      const resp = await httpClient.post(`${url}`, {
        name: data.name,
      })

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async update(id, data) {
    try {
      const resp = await httpClient.patch(`${url}/${id}`, {
        name: data.name,
      })

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async del(id) {
    try {
      const resp = await httpClient.delete(`${url}/${id}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

}