import httpClient from "./httpClient.js";
const url = 'products'

export const productApi = {

  async getAll(data) {
    try {
      let params = ``
      if (!!data?.subcategoryId) params += `subcategoryId=${data?.subcategoryId}`

      const resp = await httpClient.get(`${url}?${params}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async getBtId(id) {
    try {
      const resp = await httpClient.get(`${url}/${id}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async create(data) {
    try {
      const resp = await httpClient.post(`${url}`, {
        name: data.name,
        subcategoryId: data.subcategory.id
      })

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async update(data) {
    try {
      const resp = await httpClient.put(`${url}/${data.id}`, {
        name: data.name,
        subcategoryId: data.subcategory.id
      })
      return resp.data

    } catch (err) {
      throw err
    }
  },

  async del(id) {
    try {
      const resp = await httpClient.delete(`${url}/${id}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

}
