import axios from "axios";
import { Notify } from 'quasar';

function logOut() {
  localStorage.clear();
  window.location.href = '/login'
  location.reload()
}

const config = {
  baseURL: 'http://back.gorizontbeton.site/api/'
  // baseURL: 'http://45.9.42.8:4006/api/' http://back.gorizontbeton.site/api/
}

const httpClient = axios.create(config);
const token = localStorage.getItem('token')

if (token) {

  httpClient.interceptors.request.use(
    (config) => {
      config.headers.Authorization = `Bearer ${token}`
      return config
    }
  )

  httpClient.interceptors.response.use(function (response) {
    return response;
  }, function (error) {
    const err = error.response.status

    const originalRequest = error.config;
    if (originalRequest && !originalRequest._isRetry) {
      originalRequest._isRetry = true;

      if(err === 401) {
        console.log('Неавторизован')
        logOut()
      }
      if(err === 403) {
        console.log('Неавторизован')
        // logOut()
      }
      if(err === 404) {
        console.log('Не найдено')
      }
    }

    // Проверка на наличие message в ответе от сервера
    if (error.response && error.response.data && error.response.data.message) {
      Notify.create({
        type: 'negative', // Красный цвет для уведомления
        message: error.response.data.message,
        color: 'red',
      });
    }

    return Promise.reject(error);
  });
}

export default httpClient
