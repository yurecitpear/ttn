import httpClient from "./httpClient.js";
const url = 'ttn';

export const ttnApi = {

  async getAll(params, pagination) {
    try {
      let query = `?page=${pagination.current}&perPage=${10}`;

      if (params.id) query += `&ttnId=${params.id}`
      else {
        if (params.address) query += `&address=${params.address}`;
        if (params.data) query += `&dateCreate=${params.data}`;
      }

      const resp = await httpClient.get(`${url}${query}`);
      const { data, total, maxPages } = resp.data;

      return {
        data,
        total,
        maxPages
      }

    } catch (err) {
      throw err
    }
  },

  async getById(id) {
    try {
      const resp = await httpClient.get(`${url}/${id}`);
      const data = resp.data;
      // Обработка полученных данных, если необходимо
      return data;
    } catch (err) {
      throw err;
    }
  },

  async create(data) {
    try {
      if (data.date_delivery) {
        data.date_delivery = convertDateToISOString(data.date_delivery);
      }
      const resp = await httpClient.post(`${url}`, data);
      return resp.data;
    } catch (err) {
      throw err;
    }
  },

  async update(id, data) {
    try {
      if (data.date_delivery) {
        data.dateDelivery = convertDateToISOString(data.date_delivery);
      }
      const resp = await httpClient.patch(`${url}/${id}`, data);
      return resp.data;
    } catch (err) {
      throw err;
    }
  },

  async getCount() {
    try {
      const resp = await httpClient.get(`${url}/count`);
      return resp.data.count;
    } catch (err) {
      throw err;
    }
  },

  async del(id) {
    try {
      const resp = await httpClient.delete(`${url}/${id}`);
      return resp.data;
    } catch (err) {
      throw err;
    }
  },

};

function convertDateToISOString(dateString) {
  const [day, month, year] = dateString.split('.');
  const isoDate = new Date(year, month - 1, day);
  return isoDate.toISOString();
}
