import httpClient from "./httpClient.js";
const url = 'drivers'

export const driversApi = {

  async getAll() {
    try {
      const resp = await httpClient.get(`${url}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async getBtId(id) {
    try {
      const resp = await httpClient.get(`${url}/${id}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async searchByName(string) {
    try {
      const resp = await httpClient.get(`${url}?n=${string}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async create(data) {
    try {
      const resp = await httpClient.post(`${url}`, {
        name: data.name,
        carName: data.car_name,
        carNumber: `${data.car_number1}-${data.car_number2}`,
        carCapacity: Number(data.car_capacity),
        phone: data.phone,
      })

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async update(id, data) {
    try {
      const resp = await httpClient.put(`${url}/${id}`, {
        name: data.name,
        carName: data.car_name,
        carNumber: `${data.car_number1}-${data.car_number2}`,
        carCapacity: Number(data.car_capacity),
        phone: data.phone,
      })

      return resp.data

    } catch (err) {
      throw err
    }
  },

  async del(id) {
    try {
      const resp = await httpClient.delete(`${url}/${id}`)

      return resp.data

    } catch (err) {
      throw err
    }
  },

}
