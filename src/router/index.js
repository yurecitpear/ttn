import { route } from 'quasar/wrappers'
import { createRouter, createMemoryHistory, createWebHistory, createWebHashHistory } from 'vue-router'
import routes from './routes'

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route(function (/* { store, ssrContext } */) {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : (process.env.VUE_ROUTER_MODE === 'history' ? createWebHistory : createWebHashHistory)

  const Router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,

    history: createHistory(process.env.MODE === 'ssr' ? void 0 : process.env.VUE_ROUTER_BASE)
  })

  Router.beforeEach((to, from, next) => {
    let user = JSON.parse(localStorage.getItem('user'))
    let token = localStorage.getItem('token')
    const role = user ? user.isRole : null

    const requiresAuth = to.matched.some(record => record.meta.requireLogin)
    const requiredRoles = to.meta.isRole || []

    if (requiresAuth && !token) {
      next('/login')
    } else if (role === 'ADMIN') {
      next()
    } else if (role === 'DISPATCHER') {
      if (to.path === '/delivery') {
        next()
      } else {
        next('/delivery')
      }
    } else if (requiredRoles.length > 0 && (!role || !requiredRoles.includes(role))) {
      next('/home')
    } else {
      next()
    }
  })

  return Router
})
