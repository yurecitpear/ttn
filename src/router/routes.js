
const routes = [

  {
    path: '/login',
    component: () => import('layouts/AuthLayout.vue'),
    meta: {
      requireLogin: false,
    },
    children: [
      {
        path: '/login',
        component: () => import('pages/auth/login.vue')
      },
      {
        path: '/register',
        component: () => import('pages/auth/register.vue')
      },
      {
        path: '/reset',
        component: () => import('pages/auth/reset.vue')
      },
    ]
  },

  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    meta: {
      requireLogin: true,
    },
    children: [
      {
        path: '/home',
        component: () => import('pages/IndexPage.vue'),
        meta: {
          requireLogin: true,
        },
      },

      {
        path: '/addresses',
        component: () => import('src/pages/goods/AddressesList.vue') ,
        meta: {
          requireLogin: true,
          role: ['ADMIN']
        }
      },

      {
        path: '/drivers',
        component: () => import('src/pages/drivers/DriversList.vue') ,
        meta: {
          requireLogin: true,
          role: ['ADMIN']
        }
      },
      {
        path: '/drivers/create',
        component: () => import('src/pages/drivers/DriversCreate.vue') ,
        meta: {
          requireLogin: true,
          role: ['ADMIN']
        }
      },
      {
        path: '/drivers/:id',
        component: () => import('src/pages/drivers/DriversId.vue') ,
        meta: {
          requireLogin: true,
          role: ['ADMIN']
        }
      },

      {
        path: '/stamps',
        component: () => import('src/pages/stamps/Index.vue') ,
        meta: {
          requireLogin: true,
          role: ['ADMIN']
        }
      },
      {
        path: '/stamps/categories',
        component: () => import('src/pages/stamps/CategoriesList.vue') ,
        meta: {
          requireLogin: true,
          role: ['ADMIN']
        }
      },
      {
        path: '/stamps/subcategories',
        component: () => import('src/pages/stamps/SubcategoriesList.vue') ,
        meta: {
          requireLogin: true,
          role: ['ADMIN']
        }
      },
      {
        path: '/stamps/products',
        component: () => import('src/pages/stamps/ProductsList.vue') ,
        meta: {
          requireLogin: true,
          role: ['ADMIN']
        }
      },
      {
        path: '/stamps/factories',
        component: () => import('src/pages/stamps/FactoryList.vue') ,
        meta: {
          requireLogin: true,
          role: ['ADMIN']
        }
      },

      {
        path: '/users',
        component: () => import('src/pages/users/UsersList.vue') ,
        meta: {
          requireLogin: true,
          role: ['ADMIN']
        }
      },
      {
        path: '/users/create',
        component: () => import('src/pages/users/UsersCreate.vue') ,
        meta: {
          requireLogin: true,
          role: ['ADMIN']
        }
      },
      {
        path: '/users/:id',
        component: () => import('src/pages/users/UsersId.vue') ,
        meta: {
          requireLogin: true,
          role: ['ADMIN']
        }
      },

      {
        path: '/ttn',
        component: () => import('src/pages/ttn/TtnList.vue') ,
        meta: {
          requireLogin: true,
          role: ['ADMIN']
        }
      },
      {
        path: '/ttn/create',
        component: () => import('src/pages/ttn/TtnCreate.vue') ,
        meta: {
          requireLogin: true,
          role: ['ADMIN']
        }
      },
      {
        path: '/ttn/:id',
        component: () => import('src/pages/ttn/TtnId.vue') ,
        meta: {
          requireLogin: true,
          role: ['ADMIN']
        }
      },

      {
        path: '/quality',
        component: () => import('src/pages/quality/QualityList.vue') ,
        meta: {
          requireLogin: true,
          role: ['ADMIN']
        }
      },
      {
        path: '/quality/create',
        component: () => import('src/pages/quality/QualityCreate.vue') ,
        meta: {
          requireLogin: true,
          role: ['ADMIN']
        }
      },
      {
        path: '/quality/:id',
        component: () => import('src/pages/quality/QualityId.vue') ,
        meta: {
          requireLogin: true,
          role: ['ADMIN']
        }
      },
      {
        path: '/quality/:id',
        component: () => import('src/pages/quality/QualityId.vue') ,
        meta: {
          requireLogin: true,
          role: ['ADMIN']
        }
      },

      {
        path: '/equipment',
        component: () => import('src/pages/EquipmentList.vue') ,
        meta: {
          requireLogin: true,
          role: ['ADMIN']
        }
      },
      {
        path: '/addiotion',
        component: () => import('src/pages/AddiotionList.vue') ,
        meta: {
          requireLogin: true,
          role: ['ADMIN']
        }
      },
    ]
  },
  {
    path: '/',
    component: () => import('layouts/DispatcherLayout.vue'),
    meta: {
      requireLogin: true,
    },
    children: [
      {
        path: '/delivery',
        component: () => import('pages/DispatcherDelivery.vue'),
        meta: {
          requireLogin: true,
          role: ['ADMIN', 'DISPATCHER']
        },
      },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
