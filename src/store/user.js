import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useUserStore = defineStore('user', () => {
  // Глобальная переменная для роли пользователя
  const user = ref(JSON.parse(localStorage.getItem('user')) || null)

  return {
    user
  }
})
