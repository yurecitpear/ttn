const user = JSON.parse(localStorage.getItem('user'))

let date = new Date()
let year = date.getFullYear()
let month = (date.getMonth() + 1).toString().padStart(2, '0')
let day = date.getDate().toString().padStart(2, '0')

let formattedDate = `${year}-${month}-${day}`

// проверка доступа
export default function checkDate(data) {
  let getDate = false
  if (!!data) getDate = data.split('T')[0]
  
  if (user.role === 'admin') return true
  if (user.role === 'dispatcher' && formattedDate === getDate) return true
  if (data === false) return false
  else return false
}